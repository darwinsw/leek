﻿using System;
using System.Security.Authentication.ExtendedProtection;
using System.Threading.Tasks;
using Leek.Demo.Common;
using Leek.DependencyInversion;
using Leek.Logging;
using Leek.Outbox;
using Microsoft.Extensions.DependencyInjection;

namespace Leek.Demo.Publisher; 

class Program {
    async static Task Main(string[] args) {
        var services = new ServiceCollection();
        services.AddSingleton<PublisherApp>();
        services
            .AddMessageBus(new RabbitMqMessageBusConfiguration(
                "localhost", 5673, "leek", "leek", "Sup3sS3cret!"
            ))
            .AddDefaultMessageSerializer()
            .AddDefaultStringEncoder()
            // .AddConsoleLoggerFactory()
            // .AddLoggerFactory<MyLoggerFactory>()
            .AddLoggerFactory<NopLoggerFactory>()
            // .AddMultiPublisher<ByNameOutboxStrategy>()
            .AddMultiPublisher(new ByNameOutboxStrategy(true))
            .AddMessagePublisher<SampleMessage>(new RabbitMqPublisherConfiguration(
                new ExchangeConfiguration("demo-exchange"),
                new QueueConfiguration("demo-queue")))
            ;
            
        var provider = services.BuildServiceProvider();
        var app = provider.GetService<PublisherApp>();
        if (app != null) {
            app.Start();
            await app.Run();
            app.Stop();
        } else {
            Console.WriteLine("Error: PublisherApp not properly configured");
        }
    }
}

public record SampleMessage(string Text, long Value, DateTimeOffset When);

public record EchoMessage(string Text, DateTimeOffset When);

public record HelloMessage(string Msg, string To, DateTimeOffset When);

class PublisherApp {
    private readonly RabbitMqMessageBus bus;
    private readonly RabbitMqMessagePublisher<SampleMessage> publisher;
    private readonly RabbitMqMultiPublisher multiPublisher;

    public PublisherApp(RabbitMqMessageBus bus, RabbitMqMessagePublisher<SampleMessage> publisher,
        RabbitMqMultiPublisher multiPublisher) {
        this.bus = bus;
        this.publisher = publisher;
        this.multiPublisher = multiPublisher;
    }

    public Task Run() {
        var now = DateTimeOffset.Now;
        for (int i = 1; i <= 5; i++) {
            publisher.Publish(new SampleMessage($"Sample message #{i}", 10 * i, now.AddSeconds(i)));
        }

        multiPublisher.Publish(new EchoMessage("First sample", DateTimeOffset.Now));
        multiPublisher.Publish(new EchoMessage("Second sample", DateTimeOffset.Now));
        multiPublisher.Publish(new HelloMessage("Hello, World!", "World", DateTimeOffset.Now));
        multiPublisher.Publish(new HelloMessage("Hello, pietrom!", "pietrom", DateTimeOffset.Now));

        return Task.CompletedTask;
    }

    public void Start() {
        bus.Start();
    }

    public void Stop() {
        bus.Stop();
    }
}