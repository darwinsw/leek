﻿using System;
using System.Threading.Tasks;
using Leek.DependencyInversion;
using Leek.Inbox;
using Leek.Outbox;
using Microsoft.Extensions.DependencyInjection;

namespace Leek.Demo.Subscriber {
    class Program {
        async static Task Main(string[] args) {
            var services = new ServiceCollection();
            services
                .AddMessageBus(new RabbitMqMessageBusConfiguration(
                    "localhost", 5673, "leek", "leek", "Sup3sS3cret!"
                ))
                .AddDefaultMessageSerializer()
                .AddDefaultStringEncoder()
                .AddConsoleLoggerFactory()
                .AddMessageConsumer<MySampleMessage, MySampleMessageHandler>(new RabbitMqConsumerConfiguration(
                    new QueueConfiguration("demo-queue-consumer-0"), new ExchangeConfiguration("demo-exchange")))
                .AddMessageConsumer<MySampleMessage, MySecondSampleMessageHandler>(new RabbitMqConsumerConfiguration(
                    new QueueConfiguration("demo-queue-consumer-1"), new ExchangeConfiguration("demo-exchange")));

            var provider = services.BuildServiceProvider();
            var bus = provider.GetService<RabbitMqMessageBus>();
            if (bus != null) {
                bus.Start();
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                bus.Stop();
            } else {
                Console.WriteLine("Error: RabbitMqMessageBus not properly configured");
            }
        }
    }

    public record MySampleMessage(string Text, long Value, DateTimeOffset When);

    public class MySampleMessageHandler : MessageHandler<MySampleMessage> {
        public Task Handle(MySampleMessage msg) {
            Console.WriteLine($"Received {msg} at {DateTimeOffset.Now}");
            return Task.CompletedTask;
        }
    }

    public class MySecondSampleMessageHandler : MessageHandler<MySampleMessage> {
        public Task Handle(MySampleMessage msg) {
            Console.WriteLine($"Received [**] {msg} at {DateTimeOffset.Now}");
            return Task.CompletedTask;
        }
    }
}