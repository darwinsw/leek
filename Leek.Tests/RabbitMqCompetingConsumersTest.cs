using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leek.Inbox;
using Leek.Logging;
using Leek.Tests.TestMessages;
using NUnit.Framework;
using RabbitMQ.Client;
using static Leek.Tests.WaitingHelper;

namespace Leek.Tests;

public class RabbitMqCompetingConsumersTest {
    private RabbitMqMessageBus bus;
    private readonly Utf8Encoder encoder = new Utf8Encoder();
    private readonly JsonSerializer serializer = new JsonSerializer();
    private RabbitMqMessageBusConfiguration config;
    private IEnumerable<RabbitMqConsumer<TestMessage0>> consumers;
    private IConnection conn;
    private IModel model;
    private SimpleCountingHandler[] handlers;
    private const int ConsumersCount = 4;
    private const int Delay = 25;

    [SetUp]
    public void Setup() {
        config = RabbitMqConnectionHelper.Configuration;
        handlers = new SimpleCountingHandler[] {
            new SimpleCountingHandler("AA", Delay),
            new SimpleCountingHandler("BB", Delay),
            new SimpleCountingHandler("CC", Delay),
            new SimpleCountingHandler("DD", Delay),
        };
        consumers = handlers.Select(handler => new RabbitMqConsumer<TestMessage0>(
            new RabbitMqConsumerConfiguration(new QueueConfiguration("test-queue"),
                new ExchangeConfiguration("test-exchange")), new ConsoleLoggerFactory(), encoder, serializer,
            handler));
        bus = new RabbitMqMessageBus(config, new ConsoleLoggerFactory(),
            consumers, Enumerable.Empty<ConnectableOutbox>());
        bus.Start();
        
        conn = new ConnectionFactory {
            HostName = config.Host,
            Port = config.Port,
            VirtualHost = config.VirtualHost,
            UserName = config.Username,
            Password = config.Password
        }.CreateConnection();
        model = conn.CreateModel();
    }

    [Test]
    public async Task ShouldReceiveMessagesConcurrently() {
        
        var batch = model.CreateBasicPublishBatch();
        const int messageCount = 500;
        for (int i = 0; i < messageCount; i++)
        {
            batch.Add("test-exchange", string.Empty, false, null, new ReadOnlyMemory<byte>(encoder.Encode(serializer.Serialize(new TestMessage0("pietrom testing Leek", i, DateTimeOffset.Now)))));
        }
        batch.Publish();
        await WaitFor(TimeSpan.FromSeconds(5), TimeSpan.FromMilliseconds(300),
            () => handlers.Select(x => x.ReceivedMessagesCount).Sum() == messageCount);
        var first = handlers.Select(x => x.First).Min();
        var last = handlers.Select(x => x.Last).Max();
        var elapsed = last - first;
        Assert.That(elapsed, Is.LessThan(TimeSpan.FromMilliseconds(messageCount * Delay) / 3));
    }

    [TearDown]
    public void TearDown() {
        model.QueueDelete("test-queue");
        model.ExchangeDelete("test-exchange");
        model.Close();
        conn.Close();
        bus.Stop();
    }
}

class SimpleCountingHandler : MessageHandler<TestMessage0> {
    private readonly int delay;
    public SimpleCountingHandler(string id, int delay) {
        Id = id;
        this.delay = delay;
    }

    private string Id { get; }
    public DateTimeOffset? First { get; private set; }
    public DateTimeOffset? Last { get; private set; }
    public int ReceivedMessagesCount => received;
    private int received;

    public async Task Handle(TestMessage0 msg) {
        if(!First.HasValue) First = DateTimeOffset.Now;
        received++;
        await Task.Delay(delay);
        Last = DateTimeOffset.Now;
    }
}
