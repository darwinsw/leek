using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leek.Logging;
using Leek.Outbox;
using Leek.Tests.TestMessages;
using NUnit.Framework;
using RabbitMQ.Client;
using static Leek.Tests.WaitingHelper;

namespace Leek.Tests;

public class RabbitMqMessagePublisherTest : DefaultBasicConsumer {
    private RabbitMqMessagePublisher<TestMessage0> publisher;
    private RabbitMqMessagePublisher<TestMessage0> schedulablePublisher;

    private RabbitMqMessageBus bus;
    private IList<TestMessage0> receivedMessages;
    private readonly Utf8Encoder encoder = new Utf8Encoder();
    private readonly JsonSerializer serializer = new JsonSerializer();
    private RabbitMqMessageBusConfiguration config;

    [SetUp]
    public void Setup() {
        config = RabbitMqConnectionHelper.Configuration;
        receivedMessages = new List<TestMessage0>();
        publisher = new RabbitMqMessagePublisher<TestMessage0>(
            new RabbitMqPublisherConfiguration(new ExchangeConfiguration("test-exchange"), new QueueConfiguration("test-queue")),
            new ConsoleLoggerFactory(), encoder, serializer);
        schedulablePublisher = new RabbitMqMessagePublisher<TestMessage0>(
            new RabbitMqPublisherConfiguration(new ExchangeConfiguration("test-schedulable-exchange"),
                new QueueConfiguration("test-schedulable-queue"), CanSchedule: true), new ConsoleLoggerFactory(), encoder, serializer);
        bus = new RabbitMqMessageBus(config, new ConsoleLoggerFactory(), Enumerable.Empty<ConnectableInbox>(),
            new[] { publisher, schedulablePublisher });
        bus.Start();
    }

    [Test]
    public async Task ShouldSendMessage() {
        var now = DateTimeOffset.Now;
        var msg = new TestMessage0("pietrom testing Leek", 19, now);

        var conn = new ConnectionFactory {
            HostName = config.Host,
            Port = config.Port,
            VirtualHost = config.VirtualHost,
            UserName = config.Username,
            Password = config.Password
        }.CreateConnection();
        var model = conn.CreateModel();

        model.BasicConsume("test-queue", true, this);

        await publisher.Publish(msg);

        await WaitFor(TimeSpan.FromSeconds(2), TimeSpan.FromMilliseconds(200), () => receivedMessages.Any());
        Assert.That(receivedMessages.ToArray(), Is.EquivalentTo(new[] { msg }));
    }

    [Test]
    public async Task ShouldSendDelayedMessage() {
        var now = DateTimeOffset.Now;
        var msg = new TestMessage0("pietrom testing Leek", 19, now);

        var conn = new ConnectionFactory {
            HostName = config.Host,
            Port = config.Port,
            VirtualHost = config.VirtualHost,
            UserName = config.Username,
            Password = config.Password
        }.CreateConnection();
        var model = conn.CreateModel();

        model.BasicConsume("test-schedulable-queue", true, this);
        var delay = TimeSpan.FromMilliseconds(1200);
        await schedulablePublisher.Publish(msg, delay);
        await Task.Delay(delay - TimeSpan.FromMilliseconds(100));
        Assert.That(receivedMessages, Is.Empty);
        await WaitFor(TimeSpan.FromSeconds(2), TimeSpan.FromMilliseconds(200), () => receivedMessages.Any());
        Assert.That(receivedMessages.ToArray(), Is.EquivalentTo(new[] { msg }));
    }

    [TearDown]
    public void TearDown() {
        bus.Stop();
    }

    public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey,
        IBasicProperties properties, ReadOnlyMemory<byte> body) {
        receivedMessages.Add(serializer.Deserialize<TestMessage0>(encoder.Decode(body.ToArray())));
    }

    public Task Handle(TestMessage0 msg) {
        receivedMessages.Add(msg);
        return Task.CompletedTask;
    }
}
