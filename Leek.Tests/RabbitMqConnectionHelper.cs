using System;

namespace Leek.Tests; 

internal static class RabbitMqConnectionHelper {
    public static RabbitMqMessageBusConfiguration Configuration => new RabbitMqMessageBusConfiguration(
        Environment.GetEnvironmentVariable("TEST_RABBITMQ_HOST") ?? "localhost",
        int.Parse(Environment.GetEnvironmentVariable("TEST_RABBITMQ_PORT") ?? "5673"),
        Environment.GetEnvironmentVariable("TEST_RABBITMQ_VHOST") ?? "/",
        Environment.GetEnvironmentVariable("TEST_RABBITMQ_USERNAME") ?? "guest",
        Environment.GetEnvironmentVariable("TEST_RABBITMQ_PASSWORD") ?? "guest"
    );
}