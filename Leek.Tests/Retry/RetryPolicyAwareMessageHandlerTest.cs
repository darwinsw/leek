using System;
using System.Threading.Tasks;
using Leek.Inbox;
using Leek.Retry;
using Microsoft.VisualStudio.TestPlatform.Common.Utilities;
using Moq;
using NUnit.Framework;

namespace Leek.Tests.Retry; 

public class RetryPolicyAwareMessageHandlerTest : MessageHandler<RetryPolicyAwareMessageHandlerTest.TestMessage> {
    [Test]
    public async Task ShouldUseRetryStrategy() {
        var retryPolicy = new Mock<RetryPolicy>();
        MessageHandler<TestMessage> handler = new RetryPolicyAwareMessageHandler<TestMessage>(retryPolicy.Object, this);
        var msg = new TestMessage("pietrom", 19);
        await handler.Handle(msg);
        retryPolicy.Verify(x => x.Execute(It.IsAny<Func<Task>>()));
    }

    public record TestMessage(string Text, int Value);

    public Task Handle(TestMessage msg) {
        return Task.CompletedTask;
    }
}

