using System;
using System.Threading.Tasks;
using Leek.Retry;
using NUnit.Framework;

namespace Leek.Tests.Retry; 

public class RetryNTimesWithConstantDelayPolicyTest {
    [Test]
    public async Task WhenThereAreNoErrorsThenShouldNotRetry() {
        var counter = 0;
        RetryPolicy policy = new RetryNTimesWithConstantDelayPolicy(3, TimeSpan.FromMilliseconds(10));
        await policy.Execute(() => {
            counter++;
            return Task.CompletedTask;
        });
        Assert.That(counter, Is.EqualTo(1));
    }
    
    [Test]
    public async Task WhenThereAreErrorsThenShouldRetry() {
        var counter = 0;
        RetryPolicy policy = new RetryNTimesWithConstantDelayPolicy(3, TimeSpan.FromMilliseconds(10));
        await policy.Execute(() => {
            counter++;
            if (counter <= 1) {
                throw new Exception("Test");
            }
            return Task.CompletedTask;
        });
        Assert.That(counter, Is.EqualTo(2));
    }
    
    [Test]
    public void WhenThereAreErrorsMoreThanTimesThenShouldThrowLastException() {
        var counter = 0;
        RetryPolicy policy = new RetryNTimesWithConstantDelayPolicy(3, TimeSpan.FromMilliseconds(10));
        var thrown = Assert.ThrowsAsync<Exception>(() => policy.Execute(() => {
            counter++;
            throw new Exception($"Test {counter}");
        }));
        Assert.That(thrown.Message, Is.EqualTo($"Test 3"));
    }
}
