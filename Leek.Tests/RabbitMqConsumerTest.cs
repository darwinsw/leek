using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Leek.Inbox;
using Leek.Logging;
using Leek.Tests.TestMessages;
using NUnit.Framework;
using RabbitMQ.Client;
using static Leek.Tests.WaitingHelper;

namespace Leek.Tests;

public class RabbitMqConsumerTest : MessageHandler<TestMessage0> {
    private RabbitMqConsumer<TestMessage0> consumer;
    private RabbitMqMessageBus bus;
    private IList<TestMessage0> receivedMessages;
    private readonly Utf8Encoder encoder = new Utf8Encoder();
    private readonly JsonSerializer serializer = new JsonSerializer();
    private RabbitMqMessageBusConfiguration config;

    [SetUp]
    public void Setup() {
        config = RabbitMqConnectionHelper.Configuration;
        receivedMessages = new List<TestMessage0>();
        consumer = new RabbitMqConsumer<TestMessage0>(
            new RabbitMqConsumerConfiguration(new QueueConfiguration("test-queue"),
                new ExchangeConfiguration("test-exchange")),
            new ConsoleLoggerFactory(),
            encoder,
            serializer,
            this
        );
        bus = new RabbitMqMessageBus(config, new ConsoleLoggerFactory(),
            new[] { consumer }, Enumerable.Empty<ConnectableOutbox>());
        bus.Start();
    }

    [Test]
    public async Task ShouldReceiveMessage() {
        var now = DateTimeOffset.Now;
        var msg = new TestMessage0("pietrom testing Leek", 19, now);

        var conn = new ConnectionFactory {
            HostName = config.Host,
            Port = config.Port,
            VirtualHost = config.VirtualHost,
            UserName = config.Username,
            Password = config.Password
        }.CreateConnection();
        var model = conn.CreateModel();
        model?.BasicPublish("test-exchange", string.Empty, null, encoder.Encode(serializer.Serialize(msg)));
        await WaitFor(TimeSpan.FromSeconds(2), TimeSpan.FromMilliseconds(200), () => receivedMessages.Any());
        Assert.That(receivedMessages.ToArray(), Is.EquivalentTo(new[] { msg }));
    }
    
    [TearDown]
    public void TearDown() {
        bus.Stop();
    }

    public Task Handle(TestMessage0 msg) {
        receivedMessages.Add(msg);
        return Task.CompletedTask;
    }
}
