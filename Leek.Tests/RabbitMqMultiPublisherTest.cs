using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leek.Logging;
using Leek.Outbox;
using Leek.Tests.TestMessages;
using NUnit.Framework;
using RabbitMQ.Client;
using static Leek.Tests.WaitingHelper;

namespace Leek.Tests;

public class RabbitMqMultiPublisherTest : DefaultBasicConsumer {
    private RabbitMqMultiPublisher publisher;
    private IList<string> receivedMessages;
    private readonly Utf8Encoder encoder = new Utf8Encoder();
    private readonly JsonSerializer serializer = new JsonSerializer();
    private RabbitMqMessageBusConfiguration config;
    private IConnection? conn;

    [SetUp]
    public void Setup() {
        config = RabbitMqConnectionHelper.Configuration;
        receivedMessages = new List<string>();
        publisher = new RabbitMqMultiPublisher(
            new ConsoleLoggerFactory(),
            new ByNameOutboxStrategy(true),
            encoder,
            serializer);
        conn = new ConnectionFactory {
            HostName = config.Host,
            Port = config.Port,
            VirtualHost = config.VirtualHost,
            UserName = config.Username,
            Password = config.Password
        }.CreateConnection();
        publisher.Connect(conn);
    }

    [Test]
    public async Task ShouldSendMessages() {
        var now = DateTimeOffset.Now;
        var msg0 = new TestMessage0("pietrom testing Leek", 19, now);
        var msg1 = new TestMessage1("pietrom testing Leek", 19, now);
        var msg2 = new TestMessage2("pietrom testing Leek", 19, now);
        
        var model = conn.CreateModel();

        model.QueueDeclare(nameof(TestMessage0), true, false, false);
        model.QueueDeclare(nameof(TestMessage2), true, false, false);
        
        model.BasicConsume(nameof(TestMessage0), true, this);
        model.BasicConsume(nameof(TestMessage2), true, this);
        

        await publisher.Publish(msg0);
        await publisher.Publish(msg1);
        await publisher.Publish(msg2);

        await WaitFor(TimeSpan.FromSeconds(2), TimeSpan.FromMilliseconds(200), () => receivedMessages.Count == 2);
        Assert.That(receivedMessages.Count, Is.EqualTo(2));
        Assert.That(receivedMessages, Contains.Item(serializer.Serialize(msg0)));
        Assert.That(receivedMessages, Contains.Item(serializer.Serialize(msg2)));
    }

    [TearDown]
    public void TearDown() {
        publisher.Disconnect();
    }

    public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey,
        IBasicProperties properties, ReadOnlyMemory<byte> body) {
        receivedMessages.Add(encoder.Decode(body.ToArray()));
    }
}