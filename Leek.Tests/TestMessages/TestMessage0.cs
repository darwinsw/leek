using System;

namespace Leek.Tests.TestMessages; 

public record TestMessage0(string Text, long Value, DateTimeOffset When);
public record TestMessage1(string Message, int Value, DateTimeOffset When);
public record TestMessage2(string Msg, double Value, DateTimeOffset OccurredOn);
