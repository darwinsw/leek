using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Leek.Tests;

public static class WaitingHelper {
    public static async Task WaitFor(TimeSpan timeout, TimeSpan tryEvery, Func<Task<bool>> eval) {
        var stop = DateTimeOffset.Now + timeout;
        while (!await eval() && DateTimeOffset.Now <= stop) {
            await Task.Delay(tryEvery);
        }

        Assert.True(await eval());
    }

    public static async Task WaitFor(TimeSpan timeout, TimeSpan tryEvery, Func<bool> eval) {
        var stop = DateTimeOffset.Now + timeout;
        while (!eval() && DateTimeOffset.Now <= stop) {
            await Task.Delay(tryEvery);
        }

        Assert.True(eval());
    }
    
    public static async Task WaitFor(int maxRetryCount, TimeSpan tryEvery, Func<bool> eval) {
        int remaining = maxRetryCount;
        while (remaining > 0) {
            if (!eval()) {
                remaining--;
                if (remaining > 0) {
                    await Task.Delay(tryEvery);
                }
            }
        }

        Assert.True(eval());
    }
}
