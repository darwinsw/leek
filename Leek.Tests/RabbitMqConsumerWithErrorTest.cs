using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Leek.Inbox;
using Leek.Logging;
using Leek.Tests.TestMessages;
using NUnit.Framework;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using static Leek.Tests.WaitingHelper;

namespace Leek.Tests;

public class RabbitMqConsumerWithErrorTest : DefaultBasicConsumer, MessageHandler<TestMessage0> {
    private const string QueueName = "test-with-error-queue";
    private const string ExchangeName = "test-with-error-exchange";
    private RabbitMqConsumer<TestMessage0> consumer;
    private RabbitMqMessageBus bus;
    private IList<TestMessage0> errorMessages;
    private readonly Utf8Encoder encoder = new Utf8Encoder();
    private readonly JsonSerializer serializer = new JsonSerializer();
    private RabbitMqMessageBusConfiguration config;
    private IConnection conn;
    private IModel model;

    [SetUp]
    public void Setup() {
        config = RabbitMqConnectionHelper.Configuration;
        conn = new ConnectionFactory {
            HostName = config.Host,
            Port = config.Port,
            VirtualHost = config.VirtualHost,
            UserName = config.Username,
            Password = config.Password
        }.CreateConnection();
        model = conn.CreateModel();
        
        model.ExchangeDelete(ExchangeName, false);
        model.QueueDelete(QueueName, false, false);
        
        errorMessages = new List<TestMessage0>();
        consumer = new RabbitMqConsumer<TestMessage0>(
            new RabbitMqConsumerConfiguration(new QueueConfiguration(QueueName),
                new MoveToErrorQueueConsumerErrorHandler(),
                new ExchangeConfiguration(ExchangeName)),
            new ConsoleLoggerFactory(),
            encoder,
            serializer,
            this
        );
        bus = new RabbitMqMessageBus(config, new ConsoleLoggerFactory(),
            new[] { consumer }, Enumerable.Empty<ConnectableOutbox>());
        bus.Start();
    }

    [Test]
    public async Task WhenHandlerThrowsErrorMessageShouldBeMovedToErrorQueue() {
        var now = DateTimeOffset.Now;
        var msg = new TestMessage0("pietrom testing Leek", 19, now);

        var errorQueue = $"{QueueName}_error";
        model.QueueDeclare(errorQueue, true, false, false);
        model.BasicConsume(errorQueue, true, this);
        
        model?.BasicPublish(ExchangeName, string.Empty, null, encoder.Encode(serializer.Serialize(msg)));
        await WaitFor(TimeSpan.FromSeconds(2), TimeSpan.FromMilliseconds(200), () => errorMessages.Any());
        Assert.That(errorMessages.ToArray(), Is.EquivalentTo(new[] { msg }));
    }
    
    [TearDown]
    public void TearDown() {
        bus.Stop();
    }

    public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey,
        IBasicProperties properties, ReadOnlyMemory<byte> body) {
        errorMessages.Add(serializer.Deserialize<TestMessage0>(encoder.Decode(body.ToArray())));
    }

    public Task Handle(TestMessage0 msg) {
        throw new Exception("Test");
    }
}
