﻿using Leek.Inbox;
using Leek.Logging;
using Leek.Outbox;
using Microsoft.Extensions.DependencyInjection;

namespace Leek.DependencyInversion;

public static class LeekDependencyInversionExtensions {
    public static IServiceCollection AddMessageBus(this IServiceCollection services,
        RabbitMqMessageBusConfiguration config) {
        services.AddSingleton<RabbitMqMessageBus>();
        services.AddSingleton(config);
        return services;
    }

    public static IServiceCollection AddDefaultMessageSerializer(this IServiceCollection services) {
        services.AddSingleton<MessageSerializer, JsonSerializer>();
        return services;
    }

    public static IServiceCollection AddDefaultStringEncoder(this IServiceCollection services) {
        services.AddSingleton<StringEncoder, Utf8Encoder>();
        return services;
    }

    public static IServiceCollection AddMessageConsumer<TMsg, THandler>(this IServiceCollection services,
        RabbitMqConsumerConfiguration config)
        where THandler : class, MessageHandler<TMsg> {
        services.AddSingleton<THandler>();
        services.AddSingleton<ConnectableInbox>(p =>
            ActivatorUtilities.CreateInstance<RabbitMqConsumer<TMsg>>(p, config,
                p.GetRequiredService<
                    THandler>()
            )
        );
        return services;
    }

    public static IServiceCollection AddMultiPublisher<TStrategy>(this IServiceCollection services)
        where TStrategy : class, RabbitMqMultiPublisherOutboxStrategy {
        services.AddSingleton<RabbitMqMultiPublisher>();
        services.AddSingleton<RabbitMqMultiPublisherOutboxStrategy, TStrategy>();
        services.AddSingleton<ConnectableOutbox>(p => p.GetRequiredService<RabbitMqMultiPublisher>());
        return services;
    }

    public static IServiceCollection AddMultiPublisher(this IServiceCollection services,
        RabbitMqMultiPublisherOutboxStrategy strategy) {
        services.AddSingleton(p => ActivatorUtilities.CreateInstance<RabbitMqMultiPublisher>(p, strategy));
        services.AddSingleton<ConnectableOutbox>(p => p.GetRequiredService<RabbitMqMultiPublisher>());
        return services;
    }

    public static IServiceCollection AddMessagePublisher<TMsg>(this IServiceCollection services,
        RabbitMqPublisherConfiguration config) {
        services.AddSingleton<RabbitMqMessagePublisher<TMsg>>(p =>
            ActivatorUtilities.CreateInstance<RabbitMqMessagePublisher<TMsg>>(p, config));
        services.AddSingleton<ConnectableOutbox>(p =>
            p.GetRequiredService<RabbitMqMessagePublisher<TMsg>>());
        return services;
    }

    public static IServiceCollection AddLoggerFactory<TFactory>(this IServiceCollection services)
        where TFactory : class, LoggerFactory {
        services.AddSingleton<LoggerFactory, TFactory>();
        return services;
    }
    
    public static IServiceCollection AddConsoleLoggerFactory(this IServiceCollection services) {
        return services.AddLoggerFactory<ConsoleLoggerFactory>();
    }
}