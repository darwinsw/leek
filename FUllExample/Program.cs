﻿using Leek;
using Leek.Demo.Common;
using Leek.DependencyInversion;
using Leek.Inbox;
using Leek.Outbox;
using Microsoft.Extensions.DependencyInjection;

namespace FUllExample;

class Program {
    async static Task Main(string[] args) {
        const string exchangeName = "full-example-exchange";
        const string queueName = "full-example-queue";
        var services = new ServiceCollection();
        services
            .AddMessageBus(RabbitMqMessageBusConfigurationHelper.MessageBusConfiguration)
            .AddDefaultMessageSerializer()
            .AddDefaultStringEncoder()
            // .AddConsoleLoggerFactory()
            // .AddLoggerFactory<MyLoggerFactory>()
            .AddLoggerFactory<NopLoggerFactory>()
            .AddMessageConsumer<MyConsumerMessage0, LeekMessageHandlerToFullExampleMessageHandlerAdapter<MyConsumerMessage0>>(new RabbitMqConsumerConfiguration(
                new QueueConfiguration($"{queueName}-0"), new ExchangeConfiguration($"{exchangeName}-0")))
            .AddMessageConsumer<MyConsumerMessage1, LeekMessageHandlerToFullExampleMessageHandlerAdapter<MyConsumerMessage1>>(new RabbitMqConsumerConfiguration(
                new QueueConfiguration($"{queueName}-1"), new ExchangeConfiguration($"{exchangeName}-1")))
            .AddMessagePublisher<MyPublisherMessage0>(new RabbitMqPublisherConfiguration(
                new ExchangeConfiguration($"{exchangeName}-0")))
            .AddMessagePublisher<MyPublisherMessage1>(new RabbitMqPublisherConfiguration(
                new ExchangeConfiguration($"{exchangeName}-1")));

        services.AddSingleton<FullExampleApp>();
        services.AddSingleton<FullExampleMessagePublisher<MyPublisherMessage0>, LeekFullExampleMessagePublisher<MyPublisherMessage0>>();
        services.AddSingleton<FullExampleMessagePublisher<MyPublisherMessage1>, LeekFullExampleMessagePublisher<MyPublisherMessage1>>();
        services.AddSingleton<FullExampleMessageHandler<MyConsumerMessage0>, MyFullDemoMessageHandler0>();
        services.AddSingleton<FullExampleMessageHandler<MyConsumerMessage1>, MyFullDemoMessageHandler1>();

        var provider = services.BuildServiceProvider();
        var bus = provider.GetService<RabbitMqMessageBus>();
        if (bus != null) {
            bus.Start();
            var app = provider.GetRequiredService<FullExampleApp>();
            Console.WriteLine("Press any key to start demo...");
            Console.ReadKey();
            await app.Run();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
            bus.Stop();
        } else {
            Console.WriteLine("Error: RabbitMqMessageBus not properly configured");
        }
    }
}

class FullExampleApp {
    private readonly FullExampleMessagePublisher<MyPublisherMessage0> publisher0;
    private readonly FullExampleMessagePublisher<MyPublisherMessage1> publisher1;

    public FullExampleApp(FullExampleMessagePublisher<MyPublisherMessage0> publisher0,
        FullExampleMessagePublisher<MyPublisherMessage1> publisher1) {
        this.publisher0 = publisher0;
        this.publisher1 = publisher1;
    }

    public async Task Run() {
        for (var i = 0; i < 5; i++) {
            await publisher0.Publish(new MyPublisherMessage0("pietrom", "Full example - MSG 0", DateTimeOffset.Now, 100 + i));
            await publisher1.Publish(new MyPublisherMessage1("pietrom", "Full example - MSG 1", DateTimeOffset.Now, 1000 + i + 0.2217M));
            await Task.Delay(TimeSpan.FromMilliseconds(100));
        }
    }
}

record MyPublisherMessage0(string To, string Text, DateTimeOffset When, int Value);

record MyConsumerMessage0(string To, string Text, DateTimeOffset When, int Value);

class MyFullDemoMessageHandler0 : FullExampleMessageHandler<MyConsumerMessage0> {
    private readonly Random Rnd = new Random();
    
    public async Task Handle(MyConsumerMessage0 msg) {
        await Task.Delay(TimeSpan.FromMilliseconds(Rnd.Next(1000)));
        Console.WriteLine($"[{GetType().Name}] ==> {msg}");
    }
}
record MyPublisherMessage1(string To, string Text, DateTimeOffset When, decimal Value);

record MyConsumerMessage1(string To, string Text, DateTimeOffset When, decimal Value);

class MyFullDemoMessageHandler1 : FullExampleMessageHandler<MyConsumerMessage1> {
    private readonly Random Rnd = new Random();

    public async Task Handle(MyConsumerMessage1 msg) {
        await Task.Delay(TimeSpan.FromMilliseconds(Rnd.Next(1000)));
        Console.WriteLine($"[{GetType().Name}] ==> {msg}");
    }
}


interface FullExampleMessagePublisher<TMsg> {
    Task Publish(TMsg msg);
}

class LeekFullExampleMessagePublisher<TMsg> : FullExampleMessagePublisher<TMsg> {
    private readonly RabbitMqMessagePublisher<TMsg> leekPublisher;

    public LeekFullExampleMessagePublisher(RabbitMqMessagePublisher<TMsg> leekPublisher) {
        this.leekPublisher = leekPublisher;
    }

    public Task Publish(TMsg msg) {
        return leekPublisher.Publish(msg);
    }
}

interface FullExampleMessageHandler<TMsg> {
    Task Handle(TMsg msg);
}

class LeekMessageHandlerToFullExampleMessageHandlerAdapter<TMsg> : MessageHandler<TMsg> {
    private readonly FullExampleMessageHandler<TMsg> adaptee;

    public LeekMessageHandlerToFullExampleMessageHandlerAdapter(FullExampleMessageHandler<TMsg> adaptee) {
        this.adaptee = adaptee;
    }

    public Task Handle(TMsg msg) {
        return adaptee.Handle(msg);
    }
}





