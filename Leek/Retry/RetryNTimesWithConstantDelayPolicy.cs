using System;
using System.Threading.Tasks;

namespace Leek.Retry; 

public class RetryNTimesWithConstantDelayPolicy : RetryPolicy {
    private readonly int times;

    public RetryNTimesWithConstantDelayPolicy(int times, TimeSpan delay) {
        this.times = times;
    }

    public Task Execute(Func<Task> func) {
        var counter = times;
        while (counter > 0) {
            counter--;
            try {
                return func();
            } catch (Exception e) {
                if (counter == 0) {
                    throw;
                }
            }
        }
        return Task.CompletedTask;




    }
}
