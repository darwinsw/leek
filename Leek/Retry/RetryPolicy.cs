using System;
using System.Threading.Tasks;

namespace Leek.Retry; 

public interface RetryPolicy {
    Task Execute(Func<Task> func);
}
