using System.Threading.Tasks;
using Leek.Inbox;

namespace Leek.Retry; 

public class RetryPolicyAwareMessageHandler<TMsg> : MessageHandler<TMsg> {
    private readonly RetryPolicy retryPolicy;
    private readonly MessageHandler<TMsg> decoratee;

    public RetryPolicyAwareMessageHandler(RetryPolicy retryPolicy, MessageHandler<TMsg> decoratee) {
        this.retryPolicy = retryPolicy;
        this.decoratee = decoratee;
    }

    public Task Handle(TMsg msg) {
        return retryPolicy.Execute(() => decoratee.Handle(msg));
    }
}
