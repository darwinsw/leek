using System.Text.Json;

namespace Leek {
    public interface MessageSerializer {
        string Serialize<T>(T msg);

        T Deserialize<T>(string text);
    }
    
    public class JsonSerializer : MessageSerializer {
        private readonly JsonSerializerOptions options = new JsonSerializerOptions {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase 
        };

        public string Serialize<T>(T msg) {
            return global::System.Text.Json.JsonSerializer.Serialize(msg, options);
        }

        public T Deserialize<T>(string text) {
            return global::System.Text.Json.JsonSerializer.Deserialize<T>(text, options);
        }
    }
}