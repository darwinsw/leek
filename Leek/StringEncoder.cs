using System.Text;

namespace Leek {
    public interface StringEncoder {
        byte[] Encode(string text);
        
        string Decode(byte[] data);
    }
    
    public class Utf8Encoder : StringEncoder {
        public byte[] Encode(string text) {
            return Encoding.UTF8.GetBytes(text);
        }

        public string Decode(byte[] data) {
            return Encoding.UTF8.GetString(data);
        }
    }
}