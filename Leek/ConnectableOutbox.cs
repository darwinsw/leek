using RabbitMQ.Client;

namespace Leek {
    public interface ConnectableOutbox {
        void Connect(IConnection connection);
        
        void Disconnect();
    }
}