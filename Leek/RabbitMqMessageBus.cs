﻿using System.Collections.Generic;
using Leek.Logging;
using RabbitMQ.Client;

namespace Leek {
    public class RabbitMqMessageBus {
        private readonly Logger logger;
        private readonly IEnumerable<ConnectableInbox> inboxes;
        private readonly IEnumerable<ConnectableOutbox> outboxes;
        private readonly IConnectionFactory connectionFactory;
        private IConnection inboxConnection;
        private IConnection outboxCconnection;

        public RabbitMqMessageBus(RabbitMqMessageBusConfiguration config, LoggerFactory logFactory,
            IEnumerable<ConnectableInbox> inboxes,
            IEnumerable<ConnectableOutbox> outboxes) {
            this.logger = logFactory.GetLogger<RabbitMqMessageBus>();
            logger.Info($"Creating connection factory {config.Host}:{config.Port}/{config.VirtualHost}");
            this.inboxes = inboxes;
            this.outboxes = outboxes;
            this.connectionFactory = new ConnectionFactory() {
                HostName = config.Host,
                Port = config.Port,
                VirtualHost = config.VirtualHost,
                UserName = config.Username,
                Password = config.Password,
                // ClientProvidedName = "app:people.net",
                DispatchConsumersAsync = true
            };
        }

        public void Start() {
            logger.Info($"Starting message bus: creating outbox connection");
            outboxCconnection = connectionFactory.CreateConnection();

            logger.Info("Starting message bus: connecting outboxes");
            foreach (var outbox in outboxes) {
                outbox.Connect(outboxCconnection);
            }

            logger.Info("Starting message bus: creating inbox connection");
            inboxConnection = connectionFactory.CreateConnection();

            logger.Info("Starting message bus: connecting inboxes");
            foreach (var inbox in inboxes) {
                inbox.Connect(inboxConnection);
            }
            logger.Info("Message bus started");
        }

        public void Stop() {
            logger.Info("Stopping message bus: disconnecting inboxes");
            foreach (var inbox in inboxes) {
                inbox.Disconnect();
            }

            logger.Info("Starting message bus: closing inbox connection");
            inboxConnection?.Close();

            logger.Info("Stopping message bus: disconnecting outboxes");
            foreach (var outbox in outboxes) {
                outbox.Disconnect();
            }
            
            logger.Info("Starting message bus: closing outbox connection");
            outboxCconnection?.Close();
            logger.Info("Message bus stopped");
        }
    }
}