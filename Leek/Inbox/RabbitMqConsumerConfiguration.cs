namespace Leek.Inbox;

public record RabbitMqConsumerConfiguration(QueueConfiguration Queue,
    ConsumerErrorHandler ErrorHandler,
    ExchangeConfiguration? Exchange) {
    
    public RabbitMqConsumerConfiguration(QueueConfiguration queueConfiguration)
        : this(queueConfiguration, new NackingConsumerErrorHandler(), null) {
    }
    
    public RabbitMqConsumerConfiguration(QueueConfiguration queue, ExchangeConfiguration exchange)
        : this(queue, new NackingConsumerErrorHandler(), exchange) {
    }
}