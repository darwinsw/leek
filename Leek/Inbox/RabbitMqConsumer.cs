using System;
using System.Threading.Tasks;
using Leek.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Leek.Inbox {
    public class RabbitMqConsumer<TMsg> : ConnectableInbox {
        private readonly Logger logger;
        private readonly MessageHandler<TMsg> handler;
        private readonly StringEncoder encoder;
        private readonly MessageSerializer serializer;
        private readonly RabbitMqConsumerConfiguration cfg;
        private IModel? channel;
        private string? consumerTag;
        private readonly ConsumerErrorHandler errorHandler;

        public RabbitMqConsumer(RabbitMqConsumerConfiguration cfg, LoggerFactory logFactory, StringEncoder encoder, MessageSerializer serializer,
            MessageHandler<TMsg> messageHandler) {
            this.logger = logFactory.GetLogger<RabbitMqConsumer<TMsg>>();
            this.handler = messageHandler;
            this.encoder = encoder;
            this.serializer = serializer;
            this.cfg = cfg;
            this.errorHandler = this.cfg.ErrorHandler;
        }

        public void Connect(IConnection connection) {
            logger.Debug($"Connecting consumer {cfg}");
            channel = connection.CreateModel();
            channel.QueueDeclare(cfg.Queue.Name, true, false, false);
            if (cfg.Exchange != null) {
                channel.ExchangeDeclare(cfg.Exchange.Name, cfg.Exchange.Type.Value, true, false);
                channel.QueueBind(cfg.Queue.Name, cfg.Exchange.Name, string.Empty);
            }

            var consumer = new AsyncEventingBasicConsumer(channel);
            consumer.Received += async (ch, ea) =>
            {
                logger.Trace($"Processing message from queue {cfg.Queue.Name}");
                var body = ea.Body.ToArray();
                string text = encoder.Decode(body);
                logger.Trace($"Processing message {text} from queue {cfg.Queue.Name}");
                try {
                    TMsg msg = serializer.Deserialize<TMsg>(text);
                    await handler.Handle(msg);
                    channel.BasicAck(ea.DeliveryTag, false);
                } catch (Exception e) {
                    logger.Error($"Error consuming message\n{text}", e);
                    errorHandler.HandleError(channel, ea.DeliveryTag, cfg.Queue, e, body, ea.BasicProperties);
                }
                Task.Yield();
            };
            consumerTag = channel.BasicConsume(cfg.Queue.Name, false, consumer);
        }

        public void Disconnect() {
            logger.Debug($"Disconnecting consumer {cfg}");
            channel?.BasicCancel(consumerTag);
            channel?.Close();
        }
    }
}