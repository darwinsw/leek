using System;
using RabbitMQ.Client;

namespace Leek.Inbox;

public class NackingConsumerErrorHandler : ConsumerErrorHandler {
    public void HandleError(IModel channel, ulong deliveryTag, QueueConfiguration queue, Exception e, byte[] data, IBasicProperties props) {
        channel.BasicNack(deliveryTag, false, false);
    }
}
