using System;
using RabbitMQ.Client;

namespace Leek.Inbox;

public class MoveToErrorQueueConsumerErrorHandler : ConsumerErrorHandler {
    public void HandleError(IModel channel, ulong deliveryTag, QueueConfiguration queue, Exception e, byte[] data, IBasicProperties props) {
        var errorQueueName = $"{queue.Name}_error";
        channel.QueueDeclare(errorQueueName, true, false, false);
        channel.BasicPublish("", errorQueueName, props, data);
        channel.BasicAck(deliveryTag, false);
    }
}
