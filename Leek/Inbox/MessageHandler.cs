using System.Threading.Tasks;

namespace Leek.Inbox {
    public interface MessageHandler<in TMsg> {
        Task Handle(TMsg msg);
    }
}