using System;
using RabbitMQ.Client;

namespace Leek.Inbox;

public interface ConsumerErrorHandler {
    public void HandleError(IModel channel, ulong deliveryTag, QueueConfiguration queue, Exception e, byte[] data, IBasicProperties props);
}