namespace Leek {
    public record RabbitMqMessageBusConfiguration(
        string Host,
        int Port,
        string VirtualHost,
        string Username,
        string Password
    );
}