using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Leek.Logging;
using RabbitMQ.Client;

namespace Leek.Outbox;

public class RabbitMqMessagePublisher<TMsg> : ConnectableOutbox {
    private readonly RabbitMqPublisherConfiguration config;
    private readonly Logger logger;
    private readonly StringEncoder encoder;
    private readonly MessageSerializer serializer;
    private IModel? channel;

    public RabbitMqMessagePublisher(RabbitMqPublisherConfiguration config, LoggerFactory logFactory, StringEncoder encoder,
        MessageSerializer serializer) {
        this.config = config;
        this.logger = logFactory.GetLogger<RabbitMqMessagePublisher<TMsg>>();
        this.encoder = encoder;
        this.serializer = serializer;
    }

    public void Connect(IConnection connection) {
        logger.Debug($"Connecting {config}");
        channel = connection.CreateModel();

        if (config.CanSchedule) {
            channel?.ExchangeDeclare(config.Exchange.Name, "x-delayed-message", true, false, new Dictionary<string, object> {
                { "x-delayed-type", config.Exchange.Type.Value }
            });
        } else {
            channel?.ExchangeDeclare(config.Exchange.Name, config.Exchange.Type.Value, true, false);
        }
        
        if (config.Queue != null) {
            channel?.QueueDeclare(config.Queue.Name, true, false, false);
            channel?.QueueBind(config.Queue.Name, config.Exchange.Name, String.Empty);
        }
    }

    public void Disconnect() {
        logger.Debug($"Disconnecting {config}");
        channel?.Close();
    }

    public Task Publish(TMsg msg, TimeSpan? delay = null) {
        var text = serializer.Serialize(msg);
        logger.Trace($"Publishing message {msg} to exchange {config.Exchange.Name}");
        var data = encoder.Encode(text);
        var props = channel.CreateBasicProperties();
        if (delay != null) {
            props.Headers = new Dictionary<string, object> {
                { "x-delay", delay.Value.TotalMilliseconds }
            };
        }

        channel?.BasicPublish(config.Exchange.Name, string.Empty, props, data);
        logger.Trace($"Message {msg} published");
        return Task.CompletedTask;
    }
}
