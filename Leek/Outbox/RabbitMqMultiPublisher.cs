using System.Threading.Tasks;
using Leek.Logging;
using RabbitMQ.Client;

namespace Leek.Outbox;

public class RabbitMqMultiPublisher : ConnectableOutbox {
    private readonly Logger logger;
    private readonly RabbitMqMultiPublisherOutboxStrategy strategy;
    private readonly StringEncoder encoder;
    private readonly MessageSerializer serializer;
    private IModel? channel;

    public RabbitMqMultiPublisher(LoggerFactory logFactory, RabbitMqMultiPublisherOutboxStrategy strategy, StringEncoder encoder,
        MessageSerializer serializer) {
        this.logger = logFactory.GetLogger<RabbitMqMultiPublisher>();
        this.strategy = strategy;
        this.encoder = encoder;
        this.serializer = serializer;
    }

    public RabbitMqMultiPublisher(LoggerFactory logFactory, StringEncoder encoder, MessageSerializer serializer)
        : this(logFactory, new ByNameOutboxStrategy(), encoder, serializer) {
    }

    public void Connect(IConnection connection) {
        logger.Debug($"Connecting {nameof(RabbitMqMultiPublisher)}");
        channel = connection.CreateModel();
    }

    public void Disconnect() {
        logger.Debug($"Disconnecting {nameof(RabbitMqMultiPublisher)}");
        channel?.Close();
    }

    public Task Publish(object msg) {
        var text = serializer.Serialize(msg);
        var data = encoder.Encode(text);
        var exchange = this.strategy.ExchangeFor(msg.GetType());
        logger.Trace($"Publishing message {text} to exchange {exchange}");
        channel?.ExchangeDeclare(exchange.Name, exchange.Type.Value, true, false);
        var queue = strategy.QueueFor(msg.GetType());
        if (queue != null) {
            channel?.QueueDeclare(exchange.Name, true, false, false);
            channel?.QueueBind(queue.Name, exchange.Name, "");
        }
        channel?.BasicPublish(exchange.Name, string.Empty, null, data);
        logger.Trace($"Message published to {exchange}");
        return Task.CompletedTask;
    }
}