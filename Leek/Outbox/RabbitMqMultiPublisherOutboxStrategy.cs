using System;

namespace Leek.Outbox;

public interface RabbitMqMultiPublisherOutboxStrategy {
    ExchangeConfiguration ExchangeFor(Type msgType);
    
    QueueConfiguration QueueFor(Type msgType);
}

public class ByNameOutboxStrategy : RabbitMqMultiPublisherOutboxStrategy {
    private readonly bool bindQueues;

    public ByNameOutboxStrategy(bool bindQueues) {
        this.bindQueues = bindQueues;
    }
    
    public ByNameOutboxStrategy() : this(false) {
    }

    public ExchangeConfiguration ExchangeFor(Type msgType) {
        return new ExchangeConfiguration(msgType.Name);
    }

    public QueueConfiguration? QueueFor(Type msgType) {
        return bindQueues ? new QueueConfiguration(msgType.Name) : null;
    }
}