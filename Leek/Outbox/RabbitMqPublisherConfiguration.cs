namespace Leek.Outbox; 

public record RabbitMqPublisherConfiguration(
    ExchangeConfiguration Exchange,
    QueueConfiguration? Queue = null,
    bool CanSchedule = false
);