using RabbitMQ.Client;

namespace Leek {
    public interface ConnectableInbox {
        void Connect(IConnection connection);
        
        void Disconnect();
    }
}