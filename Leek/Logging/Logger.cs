using System;

namespace Leek.Logging; 

public interface Logger {
    void Trace(string text);
    void Debug(string text);
    void Info(string text);
    void Warn(string text);
    void Error(string text);
    void Error(string text, Exception e);
}

public class ConsoleLoggerFactory : LoggerFactory {
    public Logger GetLogger<T>() {
        return new ConsoleLogger(typeof(T));
    }
}

public class ConsoleLogger : Logger {
    private readonly string typeName;

    public ConsoleLogger(Type t) {
        typeName = t.Name;
    }

    private void Log(string level, string text) {
        Console.WriteLine($"{level} [{typeName}] {text}");
    }
    
    public void Trace(string text) {
        Log("TRACE", text);
    }

    public void Debug(string text) {
        Log("DEBUG", text);
    }

    public void Info(string text) {
        Log("INFO", text);
    }

    public void Warn(string text) {
        Log("WARN", text);
    }

    public void Error(string text) {
        Log("ERROR", text);
    }

    public void Error(string text, Exception e) {
        Error($"{text}: {e.Message}: {e.StackTrace}");
    }
}