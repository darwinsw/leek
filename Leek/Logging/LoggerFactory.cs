namespace Leek.Logging;

public interface LoggerFactory {
    Logger GetLogger<T>();
}