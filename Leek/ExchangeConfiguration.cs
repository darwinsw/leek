namespace Leek;

public record ExchangeConfiguration(string Name, ExchangeConfiguration.ExchangeConfigurationType Type) {
    public ExchangeConfiguration(string name) : this(name, ExchangeConfigurationType.Direct) {
    }

    public class ExchangeConfigurationType {
        public string Value { get; }

        private ExchangeConfigurationType(string value) {
            Value = value;
        }

        public static ExchangeConfigurationType Direct = new ExchangeConfigurationType("direct");
        public static ExchangeConfigurationType Fanout = new ExchangeConfigurationType("fanout");
        public static ExchangeConfigurationType Topic = new ExchangeConfigurationType("topic");
        public static ExchangeConfigurationType Headers = new ExchangeConfigurationType("headers");
    }
};

