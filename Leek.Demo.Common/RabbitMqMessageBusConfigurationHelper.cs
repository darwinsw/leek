using System;

namespace Leek.Demo.Common;

public static class RabbitMqMessageBusConfigurationHelper {
    public static RabbitMqMessageBusConfiguration MessageBusConfiguration =>
        new RabbitMqMessageBusConfiguration(
            Environment.GetEnvironmentVariable("TEST_RABBITMQ_HOST") ?? "localhost",
            int.Parse(Environment.GetEnvironmentVariable("TEST_RABBITMQ_PORT") ?? "5673"),
            Environment.GetEnvironmentVariable("TEST_RABBITMQ_VHOST") ?? "leek",
            Environment.GetEnvironmentVariable("TEST_RABBITMQ_USERNAME") ?? "leek",
            Environment.GetEnvironmentVariable("TEST_RABBITMQ_PASSWORD") ?? "Sup3sS3cret!"
        );
}
