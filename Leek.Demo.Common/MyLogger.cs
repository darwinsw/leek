using System;
using Leek.Logging;

namespace Leek.Demo.Common; 

public class MyLoggerFactory : LoggerFactory {
    public Logger GetLogger<T>() {
        return new MyLogger();
    }
}
    
class MyLogger : Logger {
    private void Log(string level, string text) {
        Console.WriteLine($"MyLogger {level} {text}");
    }

    public void Trace(string text) {
        Log("TRACE", text);
    }

    public void Debug(string text) {
        Log("DEBUG", text);
    }

    public void Info(string text) {
        Log("INFO", text);
    }

    public void Warn(string text) {
        Log("WARN", text);
    }

    public void Error(string text) {
        Log("ERROR", text);
    }

    public void Error(string text, Exception e) {
        Error($"{text} {e.Message}");
    }
}
