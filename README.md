# Leek
A thin, lightweight wrapper over official [RabbitMq](https://www.rabbitmq.com) drivers for .Net

### Main branch
| Feature           | Badge                                                                                                                                                                                                               |
|-------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Pipeline Status   | [![pipeline status](https://gitlab.com/darwinsw/postoffice/badges/master/pipeline.svg)](https://gitlab.com/darwinsw/leek/-/pipelines?page=1&scope=branches&ref=main)                                                                              |
| Lines coverage    | [![https://darwinsw.gitlab.io/leek/coverage/%{default_branch}/methods-coverage.svg](https://darwinsw.gitlab.io/leek/coverage/main/lines-coverage.svg)](https://darwinsw.gitlab.io/leek/coverage/main/index.htm)     |
| Branches coverage | [![https://darwinsw.gitlab.io/leek/coverage/%{default_branch}/branches-coverage.svg](https://darwinsw.gitlab.io/leek/coverage/main/branches-coverage.svg)](https://darwinsw.gitlab.io/leek/coverage/main/index.htm) |
| Methods coverage  | [![https://darwinsw.gitlab.io/leek/coverage/%{default_branch}/methods-coverage.svg](https://darwinsw.gitlab.io/leek/coverage/main/methods-coverage.svg)](https://darwinsw.gitlab.io/leek/coverage/main/index.htm)   |
| License           | [![License: GPL v2](https://img.shields.io/badge/License-GPL_v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)                                                                               |

# Developing Leek
Running `docker-compose up` in `provisioning` directory starts a RabbitMq instance responding on both `5672` (broker) and `15672` (admin console) on `localhost`.
Admin console can be accessed using standard `guest/guest` credentials.

# Using Leek
## Consuming messages
In order to consume messages from a RabbitMq instance, you can simple create an instance of `RabbitMqConsumer<TMsg>` and *connect* it through a `RabbitMQ.Client.IConnection`:
```csharp
// Specify queue name and (optionally) exchange name
var config = new RabbitMqConsumerConfiguration(
    new QueueConfiguration("test-queue"),
    new ExchangeConfiguration("test-exchange")
);

var loggerFactory = new ConsoleLoggerFactory();
var encoder = new Utf8Encoder();
var serializer = new JsonSerializer();
var handler = new MyMessageHandler();

var consumer = new RabbitMqConsumer<MyMessage>(config, loggerFactory, encoder, serializer, handler);
RabbitMQ.Client.IConnection connection = ...
consumer.Connect(connection);
```
where
```csharp
class MyMessage {
    public string Text { get; set; }
    public long Value { get; set; }
    public DateTimeOffset OccurredOn { get; set; }
}

class MyMessageHandler : MessageHandler<MyMessage> {
    public Task Handle(MyMessage msg) {
        // Do something with the message...
    }
}
```
## Producing messages
In order to publish messages you need an instance of `RabbitMqMessagePublisher` class, 
connected to *RabbitMq*:
```csharp
// Specify exchange name and (optionally) queue name
var config = new RabbitMqPublisherConfiguration(
    new ExchangeConfiguration("SampleExchange"),
    new QueueConfiguration("test")
);
var loggerFactory = new ConsoleLoggerFactory();
var encoder = new Utf8Encoder();
var serializer = new JsonSerializer();

var publisher = new RabbitMqMessagePublisher<MyMessage>(config, loggerFactory, encoder, serializer);
RabbitMQ.Client.IConnection connection = ...
publisher.Connect(connection);
var MyMessage = new MyMessage { ... };
publisher.Publish<>();
```
where
```csharp
class MyMessage {
    public string Text { get; set; }
    public long Value { get; set; }
    public DateTimeOffset OccurredOn { get; set; }
}
```
## MessageBus: wiring all together
`RabbitMqMessageBus` is the class responsible to manage lifecycle and hold reference to two
instances of `RabbitMQ.Client.IConnection`, one for incoming and one for outgoing messages: it
creates them when its `Start` method is invoked and passes them to *publishers* and *consumers*.

```csharp
var loggerFactory = new ConsoleLoggerFactory();
var encoder = new Utf8Encoder();
var serializer = new JsonSerializer();

var pub0 = new RabbitMqMessagePublisher<SampleMessage0>(new RabbitMqPublisherConfiguration(
    new ExchangeConfiguration("SampleExchange0"),
    new QueueConfiguration("SampleQueue0")
), loggerFactory, encoder, serializer);

var pub1 = new RabbitMqMessagePublisher<SampleMessage1>(new RabbitMqPublisherConfiguration(
    new ExchangeConfiguration("SampleExchange1"),
    new QueueConfiguration("SampleQueue1")
), loggerFactory, encoder, serializer);

var consumer0 = new RabbitMqConsumer<MyMessage>(new RabbitMqConsumerConfiguration(
    new QueueConfiguration("SampleQueue0")
), loggerFactory, encoder, serializer, new SampleMessage0Handler());

var consumer1 = new RabbitMqConsumer<MyMessage>(new RabbitMqConsumerConfiguration(
    new QueueConfiguration("SampleQueue1")
), loggerFactory, encoder, serializer, new SampleMessage1Handler());

var bus = new RabbitMqMessageBus(new RabbitMqMessageBusConfiguration {
    ...
}, loggerFactory, new ConnectableInbox[] { consumer0, consumer1 }, new ConnectableOutbox[] { pub0, pub1 });

bus.Start();
```
where
```csharp
class SampleMessage0Handler : MessageHandler<SampleMessage0> {
    ...
}

class SampleMessage1Handler : MessageHandler<SampleMessage1> {
    ...
}
```
## Dependeny injection
In order to reduce the complexity of interacting with such low level API when configuring your
publishers and subscribers, you can rely upon many *extension methods*, designed to simplify 
Leek's  component registration into you application's IoC container:

Adding the package [Leek./DependencyInversion](https://www.nuget.org/packages/Leek.DependencyInversion)
to your main project's dependencies you can write 
```csharp
services
    .AddMessageBus(new RabbitMqMessageBusConfiguration(
        "localhost", 5672, "leek", "leek", "Sup3sS3cret!"
    ))
    .AddDefaultMessageSerializer()
    .AddDefaultStringEncoder()
    .AddConsoleLoggerFactory()
    .AddMessageConsumer<SampleMessage0, SampleMessage0Handler>(new RabbitMqConsumerConfiguration(
        new QueueConfiguration("SampleQueue0"), new ExchangeConfiguration("SampleExchange0")))
    .AddMessagePublisher<SampleMessage0>(new RabbitMqPublisherConfiguration(
        new ExchangeConfiguration("SampleExchange0"), new QueueConfiguration("SampleQueue0")))
    
    ;
```
into you `ConfigureServices` method.

Then you can inject `RabbitMqMessageBus` into your app's  initialization code
(maybe a class implementing `IHostedService`?) and calling its `Start()` method.

## Hexagonal architecture